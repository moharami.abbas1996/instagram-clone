# Instagram clone

<p align="center">Fullstack Instagram clone</p>

<a href="https://gitlab.com/abbasmdev/instagram-clone/-/commits/master"><img alt="pipeline status" src="https://gitlab.com/abbasmdev/instagram-clone/badges/master/pipeline.svg" /></a>

## Structure

| Codebase             |   Description    |
| :------------------- | :--------------: |
| [Mazichal](mazichal) | Next.js Frontend |
| [Damavand](damavand) | Nest.js Backend  |
